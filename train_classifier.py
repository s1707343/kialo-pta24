from argparse import ArgumentParser

import pandas as pd
import numpy as np

from sklearn.metrics import precision_recall_fscore_support, accuracy_score

from transformers import AutoModelForSequenceClassification, AutoTokenizer, DataCollatorWithPadding, Trainer, TrainingArguments
from transformers.generation.logits_process import LogitsProcessor
from datasets import Dataset, DatasetDict

# df = pd.read_pickle('data.pkl')[['premise', 'conclusion', 'prem_substance', 'conc_substance', 'assigned_annotator']]
# df_conc = df.drop(['premise', 'prem_substance'], axis=1).rename({'conclusion':'text', 'conc_substance':'substance'}, axis=1)
# df_prem = df.drop(['conclusion', 'conc_substance'], axis=1).rename({'premise':'text', 'prem_substance':'substance'}, axis=1)

# df = pd.concat([df_conc, df_prem], axis=0, ignore_index=True).sample(frac=1, ignore_index=True)

# df['substance'] = df['substance'].astype('category')
# df['labels'] = df['substance'].cat.codes.astype(int)
# df = df.drop('substance', axis=1)

# test_ds = df[df['assigned_annotator'] == 4]
# temp_ds = df[df['assigned_annotator'] != 4]
# val_ds = temp_ds.sample(frac=0.1)
# train_ds = temp_ds.loc[temp_ds.index.difference(val_ds.index)]


# dataset = DatasetDict({'train': Dataset.from_pandas(train_ds), 'val': Dataset.from_pandas(val_ds), 'test': Dataset.from_pandas(test_ds)})

# dataset.save_to_disk("dataset_classification.hf")

def preprocess_function(examples):    
    model_inputs = tokenizer(examples["text"],truncation=True, padding=True)
    # labels = tokenizer(examples['labels'], padding=True, truncation=True)
    model_inputs['labels'] = examples['labels']
    return model_inputs





def compute_metrics(eval_pred):
    predictions, labels = eval_pred

    predictions = np.argmax(predictions, axis=-1)
    results = {}
    p, r, f1, _ = precision_recall_fscore_support(labels, predictions, average='macro')
    acc = accuracy_score(labels, predictions)
    results['precision'] = p
    results['recall'] = r
    results['f1'] = f1
    results['accuracy'] = acc
    # Extract a few results

    results = {key: value * 100 for key, value in results.items()}
    # Add mean generated length
    
    return results



if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--dataset", default="dataset_classification.hf")
    parser.add_argument("--model", default="bert-large-uncased")
    parser.add_argument("--epochs", type=int, default=15)
    
    args = parser.parse_args()
    df = pd.read_pickle('data/kial-pta24.pkl')
    df1  = df.drop(['conc_substance', 'conclusion'], axis=1).rename({'prem_substance':'substance', 'premise':'rewritten_text'}, axis=1)
    df2 = df.drop(['prem_substance', 'premise'], axis=1).rename({'conc_substance':'substance', 'conclusion':'rewritten_text'}, axis=1)
    df = pd.concat([df1, df2], axis=0)
    
    dataset = {}
    dataset['train'] = Dataset.from_pandas(df[df['split'] == 'train'])
    dataset['val'] = Dataset.from_pandas(df[df['split'] == 'val'])
    dataset = DatasetDict(dataset)

    model_name = args.model.split('/')[-1]

    model = AutoModelForSequenceClassification.from_pretrained(args.model, num_labels=3)
    tokenizer = AutoTokenizer.from_pretrained(args.model)

    tokenized_dataset = dataset.map(preprocess_function, batched=True)
    data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

    training_args = TrainingArguments(
        output_dir="classification+"+model_name,
        learning_rate=1e-5,
        per_device_train_batch_size=16,
        per_device_eval_batch_size=32,
        num_train_epochs=args.epochs,
        weight_decay=0.01,
        evaluation_strategy = 'epoch',
        logging_steps = 10,
        logging_dir = f"classification+{model_name}/logs",
        save_strategy='epoch'
        )


    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=tokenized_dataset['train'],
        eval_dataset=tokenized_dataset['val'],
        compute_metrics=compute_metrics,
        data_collator=data_collator,
        tokenizer=tokenizer,
        )

    
    trainer.train()

