# ArgNotator

This is an annotation tool to for annotating arguments according the [Argument Type Identification Procedure (ATIP)](https://periodic-table-of-arguments.org/argument-type-identification-procedure/) by Jean Wagemans.

## Setup

1. Clone the repository  onto the machine you wish to use as the server
2. Create a new environment using conda create -n argnotator
3. Install the required dependencies python dependencies `pip install -r requirements.txt`
4. Install the node.js dependencies using `yarn install`
5. Modify the URL and port to the desired values in the `package.json` file
6. Create an annotator profile using `./add-user.sh USER_NAME ANNOTATOR_ID`

## Accessing the Tool

1. Launch the argnotator conda environment
2. Run the backend API using `yarn start backend`
3. In a new terminal run the web app using `yarn start`
4. Access the tool using your web-browser. The default address is `localhost:5173`.
5. Login using the desired USER_NAME

## Uploading custom datasets

To use custom datasets you will need to replace the `annotation_data.csv` file in the `data/` directory. This file will require the following columns:
| pairid  | text_to |text_from	| annotator	| topic |
|---------|---------|---------|---------|---------|
| A unique number associated with the pair of statements to be annotated | The sentence that contains the main claim/conclusion | The sentence providing supporting reasons | The anntotator id you wish to assign to the sample | The topic the pair of statements fall under |
