from flask import render_template, Flask, Response, request
import json

from .utils import *
import os
import sys

data_loader = load_data()

app = Flask(__name__)



@app.route('/api/get-args')
def getArgs():
	arg_id = request.args['arg_id']
	user_id = request.args['user_id']
	user_data = data_loader[(data_loader['annotator'] == int(user_id)) | (data_loader['annotator'] == 4) ]
	data = user_data.loc[arg_id]
	return Response(data.to_json(), mimetype='application/json')


@app.route('/api/get-list')
def getArgList():
	user_id = int(request.args['user_id'])
	data = data_loader[(data_loader['annotator'] == int(user_id)) | (data_loader['annotator'] == 4) ].index.to_series()
	return Response(data.to_json(orient='records'), mimetype='application/json')


@app.route('/api/check-completed')
def checkCompleted():
	user = request.args['user']
	files = [os.path.splitext(f)[0] for f in os.listdir(f'./data/annotations/{user}') if ('.ann' in f)]
	return Response(json.dumps(files), mimetype='application/json')

@app.route('/api/submit-annotation', methods = ["GET", "POST"])
def submitForm():
	data = request.json
	arg_id = data['arg_id']
	user = data['user']
	wasSkipped = data.get('skipped')
	with open(f'./data/annotations/{user}/{arg_id}.ann', 'w') as f:
		json.dump(data['annotation'], f)

	if wasSkipped:
		with open(f'./data/annotations/{user}/skipped.txt','r+') as f:
		    data = ''.join([i for i in f if i != id]) #1
		    f.seek(0)                                                         #2
		    f.write(data)                                                     #3
		    f.truncate()                                                      #4


	return Response(json.dumps({'success':True}), 200, mimetype='application/json')


@app.route('/api/skip')
def skip():
	arg_id = request.args['arg_id']
	user = request.args['user']
	with open(f'./data/annotations/{user}/skipped.txt', 'r+') as f:
		skipped = f.read().splitlines()
		if arg_id not in skipped:
			skipped.append(arg_id)
			print(skipped)
			f.seek(0)
			f.write('\n'.join(skipped))

	ann_file = f'./data/annotations/{user}/{arg_id}.ann'
	if os.path.exists(ann_file):
		os.remove(ann_file)
	return Response(json.dumps({'success':True}), 200, mimetype='application/json')


@app.route('/api/get-skipped')
def retrieveSkipped():
	user = request.args['user']
	try:
		with open(f'./data/annotations/{user}/skipped.txt', 'r') as f:
			skipped = [line.strip() for line in f]
	except FileNotFoundError as e:
		with open(f'./data/annotations/{user}/skipped.txt', 'w') as f:
			skipped = []
	return Response(json.dumps(skipped), mimetype='application/json')


@app.route('/api/get-annotations')
def getAnnotations():
	id = request.args['id']
	user = request.args['user']
	with open(f'./data/annotations/{user}/{id}.ann', 'r') as f:
		data = json.load(f)

	return Response(json.dumps(data), mimetype='application/json')


@app.route('/api/exists-user')
def checkExists():
	name = request.args['name'].strip()
	exists = False
	id = None
	with open('./data/users.txt', 'r') as f:
		for line in f:
			try:
				n, id = line.split()
			except:
				break

			if name == n:
				exists = True
				break

	return Response(json.dumps({'exists': exists, 'id': id}), mimetype='application/json')
