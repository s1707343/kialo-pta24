import os
import json
import numpy as np
import pandas as pd

DATA_DIR = './data/supports_combined.csv'


def load_data():
    return pd.read_csv('./data/annotation_data.csv', index_col=1)
