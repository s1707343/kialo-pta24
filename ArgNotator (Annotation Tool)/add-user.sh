#!/bin/bash

username=$1
id=$2

if [ -d "data/annotations/$username" ]
then
	echo "User $username already exists"
else
	mkdir data/annotations/$username
	echo -e "$username $id" >> data/users.txt
	touch data/annotations/$username/skipped.txt
fi
