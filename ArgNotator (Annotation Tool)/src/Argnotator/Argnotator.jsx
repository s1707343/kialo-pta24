import React, { Component } from 'react';
import reactStringReplace from 'react-string-replace';
import regeneratorRuntime from "regenerator-runtime";




function escapeRegExp(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}


function TextBox(props) {

  // const arg1Styled =
  // const arg2Styled = <span class="arg2" style="color:blue">{arg2}</span>;
  function highlightSpan(part, premise, conclusion) {
    if (part.toLowerCase() === premise.toLowerCase()) return { background: "#B0E0E6" };
    if (part.toLowerCase() === conclusion.toLowerCase()) return  { background: "#C6E2FF" };
    return {};
  }

  function getHighlightedText(text, premise, conclusion) {
      // Split on highlight term and include term into parts, ignore case
      if (text == null) {return text};

      premise = premise == null ? "" : premise;
      conclusion = conclusion == null ? "" : conclusion;
      let parts;


      if (premise == "") {
        if (conclusion != "") {
          parts = text.split(new RegExp(`(${escapeRegExp(conclusion)})`, 'gi'));
        }
        else {parts = [text]};
      }
      else if (conclusion == "") {
        if (premise != "") {
          parts = text.split(new RegExp(`(${escapeRegExp(premise)})`, 'gi'));
        }
        else {parts = [text]};
      }

      else {parts = text.split(new RegExp(`(${escapeRegExp(premise)}|${escapeRegExp(conclusion)})`, 'gi'))}



      return <span> { parts.map((part, i) =>
          <span key={i} style={highlightSpan(part, premise, conclusion)}>
              { part }
          </span>)
      } </span>;
  }

  let arg1 = getHighlightedText(props.arg1, props.premise, props.conclusion);
  let arg2 = getHighlightedText(props.arg2, props.premise, props.conclusion);
  return (<>
  <span style={{color:"teal", fontSize:"15pt"}}>Topic: {props.topic}<br /></span>
  <div className="bg-light rounded">
  <div className="container ml-1" id="raw-text">
  <span style={{color:"indigo"}}>{arg1}<br /></span>
  <span style={{color:"purple"}}>{arg2}<br /></span>
  </div>
   </div></>
 )
}

function HighlightButtons(props){
  return (         <div className="btn-group w-25" role="group">
                   <button type={"button"} className="btn" style={{background: ( props.active == "premise" ? "#B0E0E6" : "") }} onClick={props.premiseHandler} aria-pressed={ props.active == "premise"}> Premise </button>
                   <button type={"button"} className="btn" style={{background: ( props.active == "conclusion" ? "#C6E2FF" : "")}} onClick={props.conclusionHandler} aria-pressed={ props.active == "conclusion"}> Conclusion </button>

                   </div>
                 )
}

function args(arg1, arg2){
  return (<div className="args"> {arg1} {arg2} </div>)
}

function AlphaRewrite(props){
  let final = [];

  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="a" name="a" id="a_form" placeholder="a" value={props.value ? props.value.a : ""}></textarea>)
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="x" name="X" id="X_form" placeholder="X" value={props.value ? props.value.x : ""}></textarea>)
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="y" name="Y" id="Y_form" placeholder="Y" value={props.value ? props.value.y : ""}></textarea>)
  return final;
}

function BetaRewrite(props){
  let final = [];
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="a" name="a" id="a_form" placeholder="a" value={props.value ? props.value.a : ""}></textarea>)
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="b" name="b" id="b_form" placeholder="b" value={props.value ? props.value.b : ""}></textarea>)
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="x" name="X" id="X_form" placeholder="X" value={props.value ? props.value.x : ""}></textarea>)
  return final;
}

function GammaRewrite(props){
  let final = [];
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="a" name="a" id="a_form" placeholder="a" value={props.value ? props.value.a : ""}></textarea>)
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="b" name="b" id="b_form" placeholder="b" value={props.value ? props.value.b : ""}></textarea>)
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="x" name="X" id="X_form" placeholder="X" value={props.value ? props.value.x : ""}></textarea>)
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="y" name="Y" id="Y_form" placeholder="Y" value={props.value ? props.value.y : ""}></textarea>)
  return final;

}

function DeltaRewrite(props){
  let final = [];
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="a" name="a" id="a_form" placeholder="a" value={props.value ? props.value.a : ""}></textarea>)
  final.push(<textarea onChange={props.handler} className="form-control" type="text" key="r" name="R" id="R_form" placeholder="R" value={props.value ? props.value.r : ""}></textarea>)
  return final;

}



function Substance(props){
  return (

    <fieldset>
    <legend className="col-form-label" for={props.name}> {props.title} </legend>

    <div className="form-check" id={props.name}>
    <input className="form-check-control" type="radio" id={props.name+"+value"} name={props.name} onChange={props.handler} value="value" checked={props.substance === "value"}/>
    <label className="form-check-label" for={props.name+"+value"}>Value</label>
    </div>

    <div className="form-check">
    <input className="form-check-control" type="radio" id={props.name+"+fact"} name={props.name} onChange={props.handler} value="fact" checked={props.substance === "fact"}/>
    <label className="form-check-label" for={props.name+"+fact"}>Fact</label>
    </div>

    <div className="form-check">
    <input className="form-check-control" type="radio" id={props.name+"+policy"} name={props.name} onChange={props.handler} value="policy" checked={props.substance === "policy"}/>
    <label className="form-check-label" for={props.name+"+policy"}>Policy</label>
    </div>
    </fieldset>
  )
}

function ArgumentForm(props){
  return (
    <div className="form-group">
    <div className="form-check">
    <input className="form-check-control" type="radio" id="alpha" name="argument_form" value="alpha" onChange={props.handler} checked={props.form === "alpha"}/>
    <label className="form-check-label" for="alpha">Alpha</label>
    </div>

    <div className="form-check">
    <input className="form-check-control" type="radio" id="beta" name="argument_form" value="beta" onChange={props.handler} checked={props.form === "beta"}/>
    <label className="form-check-label" for="beta">Beta</label>
    </div>

    <div className="form-check">
    <input className="form-check-control" type="radio" id="gamma" name="argument_form" value="gamma" onChange={props.handler} checked={props.form === "gamma"}/>
    <label className="form-check-label" for="gamma">Gamma</label>
    </div>

    <div className="form-check">
    <input className="form-check-control" type="radio" id="delta" name="argument_form" value="delta" onChange={props.handler} checked={props.form === "delta"}/>
    <label className="form-check-label" for="delta">Delta</label>
    </div>
    </div>
  )
}

function type2Lever(type){
  switch(type){
    case("correlation"):
      return "is correlated with"
    case("effect"):
      return "is an effect of"
    case("cause"):
      return "causes"
    case("sign"):
      return "is a sign of"
    case("criterion"):
      return "is a criterion for"
    case("definition"):
      return "is the definition of"
    case("axiological"):
      return "is a moral justification for"
    case("standard"):
      return "is a standard for"
    case("pragmatic"):
      return "is a pragmatic justification for"
    case ("deontic"):
      return "is a deontic justification for"
    case ("evaluation"):
      return "is an evaluation criteria for"
    case ("case to case"):
      return "is a precedent for"
    case ("example"):
      return "is an example of"
    case ("genus"):
      return "is an established classification of"
    case ("similarity"):
      return "is similar to"
    case ("a minore"):
      return "is a specific case of"
    case ("a maiore"):
      return "is a generalisation of"
    case ("analogy"):
      return "is analogous to"
    case ("equality"):
      return "is equal to"
    case ("parallel"):
      return "is a parallel for"
    case ("comparison"):
      return "is comparable to"
    case ("consistency"):
      return "is consistent with"
    case ("opposites"):
      return "is the opposite of"
    case ("disjunctives"):
      return "is disjunctive with"
    case ("petitio principii"):
      return "is assumed by"
    case ("tradition"):
      return "is a traditional justification for"
    default:
      return type
  }
}

function Lever(props){
  let conc_lever;
  let prem_lever;
  switch (props.form) {

    case ("alpha"):
      conc_lever = props.canonical_form.x;
      prem_lever = props.canonical_form.y;
      break;
    case ("beta"):
      conc_lever = props.canonical_form.a;
      prem_lever = props.canonical_form.b;
      break;
    case ("gamma"):
      conc_lever = props.canonical_form.a + " " + props.canonical_form.x;
      prem_lever = props.canonical_form.b + " " + props.canonical_form.y;
      break;
    case ("delta"):
      conc_lever = props.canonical_form.r;
      prem_lever = "being true";
      break;
  }


  if (props.type && props.type != "n/a" && props.type != "delta") {
   return (<>
     <div className="form-text" for="validation-group"> Is the following true?
     <div className="bg-light rounded">
     <div className="container ml-1">
     <span style={{color:"purple"}}>{prem_lever}</span> <span style={{color:"orange"}}>{type2Lever(props.type).toUpperCase()}</span> <span style={{color:"indigo"}}>{conc_lever}</span>

     </div> </div> </div>

     <div className="form-group" id="validation-group">

   <div className="form-check">
   <input className="form-check-control" type="radio" id="valid-yes" name="valid-arg" value="yes" onChange={props.handler}  checked={props.is_valid === "yes"}/>
   <label className="form-check-label" for="valid-yes">Yes</label>
   </div>
   <div className="form-check">
   <input className="form-check-control" type="radio" id="valid-no" name="valid-arg" value="no" onChange={props.handler} checked={props.is_valid === "no"}/>
   <label className="form-check-label" for="valid-no" >No</label>
   </div>
   <div className="form-check">
   <input className="form-check-control" type="radio" id="valid-ns" name="valid-arg" value="n/a" onChange={props.handler} checked={props.is_valid === "n/a"}/>
   <label className="form-check-label" for="valid-ns" >Not sure</label>
   </div>
   </div>
   </>
 )
}
else {
return null
}
}

function ListItem(props){
  let cname;
  if (props.completed)
  {cname = "btn-block btn-outline-secondary";}
  else if (props.skipped)
  {cname = "btn-block btn-outline-danger";}
  else
  {cname = "btn-block btn-outline-primary";};

  cname = cname + (props.active ? " active" : "");
  return <div><button onClick={props.handler} type="button" className={cname}>{props.value}</button></div>
}


function ScrollingList(props){
  let rows = [];
  let active;
  let completed;
  let skipped;
  for (let i = 0; i < props.rows.length; i++) {
    active = false;
    completed = false;
    skipped = false;

    if (props.active === i){
      active = true
    };
    if (props.completed.includes(props.rows[i])){
      completed = true
    };
    if (props.skipped.includes(props.rows[i])) {
      skipped = true
    };



    rows.push(<ListItem key={i} value={props.rows[i]} handler={() => props.handler(i)} active={active} completed={completed} skipped={skipped}/>);
  }
  return (
    <div className="bg-light border-right" id="sidebar-wrapper">
        <div className="list-group list-group-flush overflow-auto vh-100">
        {rows}
        </div>
        </div>
)
}


function argTypes(form, csub, psub){
  const types = {alpha:{
      ff: ['Correlation', 'Effect', 'Cause', 'Sign'],
      vf: ['Criterion'],
      vv: ['Axiological', 'Standard'],
      pf: ['Pragmatic'],
      pv: ['Deontic', 'Evaluation']
    },
    beta:{
      ff: ['Case to Case', 'Example', 'Genus', 'Similarity'],
      vv: ['A Maiore', 'A Minore', 'Analogy', 'Parallel'],
      pf: ['Parallel'],
      pp: ['Comparison']
    },
    gamma:{
      vf: ['Tradition'],
      vv: ['Opposites', 'Disjunctives', 'Petitio Principii'],
      pf: ['Consistency'],
      fv: ['Opposites', 'Disjunctives', 'Petitio Principii'],
      ff: ['Opposites', 'Disjunctives', 'Petitio Principii'],


    },
    delta:{
      vf: ['Authority', 'Ad Populum', 'Commitment'],
      vv: ['Utility', 'Beauty'],
      pf: ['Ad Baculum', 'Ad Carotam'],
      pv: ['Character', 'Ad Hominem', 'Ethotic'],
      pp: ['Emotion']
    }
  };

  if (csub && psub){
    const type = types[form][csub[0] + psub[0]];
    if (type) {return type};
  };

  return []
}

function TypeDropdown(props){
  const options = [<option key="na" value='n/a'>N/A</option>];
  props.types.map((type) => options.push(<option key={type} value={type.toLowerCase()}>{type}</option>));

  return (  <div className="col-xs-2 w-25">
  <label className="form-text" for="type-select"> Argument Type </label>

<select className="form-control" onChange={props.handler} id="type-select" value={props.value}> {options} </select></div>)
}

function intersection(array1, array2){
  return array1.filter(value => array2.includes(value));

}




async function getArgList(user_id) {
  return ( await fetch(`api/get-list?user_id=${user_id}`).then(response => response.json()))
}



async function getArgData(user_id, arg_id) {
  return ( await fetch(`api/get-args?arg_id=${arg_id}&user_id=${user_id}`)
        .then(response => response.json())

      )
}






class Argnotator extends Component {
  // const [state, setState] = useState();
  //
  // fetch('/getData')
  //       .then(response => response.json())
  //       .then(data => setData(data);
  //


  constructor(props) {
  super(props);
  this.state = {annotation: {}, argData: {}, ix: 0, completed: [], skipped: [], enableSubmit: false, activeHighlight: "premise"};
  this.argList = [];
  this.defaultAnnotation = {highlightedPremise: "", highlightedConclusion: "", premise: "", conclusion: "", form: "alpha", prem_substance: "", conc_substance: "", canonical_form: {a:"", x:"", y:"" }, type: 'n/a', is_valid: null}
  this.onFormChange = this.onFormChange.bind(this);
  this.onListClick = this.onListClick.bind(this);
  this.onNext = this.onNext.bind(this);
  this.onPrev = this.onPrev.bind(this);
  this.onSubmit = this.onSubmit.bind(this);
  this.onSubstanceChange = this.onSubstanceChange.bind(this);
  this.onSkip = this.onSkip.bind(this);
  this.user = props.user;
  this.user_id = props.user_id;
  this.canonicalForm = this.canonicalForm.bind(this);
  this.onSelectType = this.onSelectType.bind(this);
  this.onValid = this.onValid.bind(this);
  this.highlightSelection = this.highlightSelection.bind(this);
  this.onClickHighlightPremise = this.onClickHighlightPremise.bind(this);
  this.onClickHighlightConclusion = this.onClickHighlightConclusion.bind(this);

}



  componentDidMount() {
    getArgList(this.user_id).then(data => {this.argList = data; return data}).then(data =>
    getArgData(this.user_id, this.argList[this.state.ix]).then(data => this.setState({argData:data})));

    this.getCompleted()
      .then(completed => {this.setState({completed: intersection(completed, this.argList)});
        return completed;
      })
      .then(completed => {return this.getAnnotations(this.argList[this.state.ix])})
      .then(annotation => {this.setState({annotation: annotation})});

    this.getSkipped().then(skipped => {this.setState({skipped: intersection(skipped, this.argList)})});
    document.addEventListener("keydown", this.handleKeypress)

    document.getElementById('raw-text').addEventListener("mouseup", this.handleSelectText)
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeypress)
  }


  onFormChange(event){
    let annotation = this.state.annotation;
    annotation["form"] = event.target.value;
    annotation["type"] = "n/a"
    this.setState({annotation:annotation});
    this.validateForm();

  };

  getCompleted = async () => {
    return  ( await fetch(`api/check-completed?user=${this.user}`).then(response => response.json()))
  };


  setSkipped = async (id) =>{
    return ( await fetch(`api/skip?arg_id=${id}&user=${this.user}`))
  };

  getSkipped = async () => {
     return ( await fetch(`api/get-skipped?user=${this.user}`)
          .then(response => response.json())
        )
  };

  getAnnotations = async (id) => {
    if (this.state.completed.includes(id)) {
    return ( await fetch(`api/get-annotations?id=${id}&user=${this.user}`)
          .then(response => response.json())

        )
      }
      else {
        return JSON.parse(JSON.stringify(this.defaultAnnotation))
        ;
      }
  };

  handleSelectText = (event) => this.highlightSelection();

  handleKeypress = (event) => {

    const textBoxes = document.getElementsByTagName("textarea");
    for (const box of textBoxes) {
      if (event.target.id == box.id){
        return;
      }
    }
    switch(event.key){
      case('ArrowLeft'):
        if (this.state.ix > 0){
          this.onPrev();
        }
        break;
      case('ArrowRight'):
        if (this.state.ix < this.argList.length - 1){
          this.onNext();
        }
        break;
      // case('Enter'):
      //   if (this.state.enableSubmit){
      //     this.onSubmit();
      //   }
      //   break;
    }
  }


   highlightSelection() {
    let parent = window.getSelection().focusNode.parentElement.closest("#raw-text");
    if (this.state.activeHighlight == null) {return};
    if(parent == null) {return};
    if(parent.id != "raw-text") {return};

    var userSelection = window.getSelection().toString();
    let annotation = this.state.annotation;
    if (this.state.activeHighlight == "premise"){  annotation['highlightedPremise'] = userSelection}
    else if (this.state.activeHighlight == "conclusion"){  annotation['highlightedConclusion'] = userSelection};
    this.setState({annotation: annotation});

  }


  onSubstanceChange(event){
    let annotation = this.state.annotation;
    if(event.target.name === "premise_substance"){
      annotation['prem_substance'] = event.target.value;
    }
    else{
      annotation['conc_substance'] = event.target.value;
    }
    this.setState({annotation: annotation})
    this.validateForm();
  }

  onClickHighlightPremise(){
    if (this.state.activeHighlight == "premise") {this.setState({activeHighlight: null})}
    else {this.setState({activeHighlight: "premise"})};
    console.log(this.state.activeHighlight);
  }

  onClickHighlightConclusion(){
    if (this.state.activeHighlight == "conclusion") {this.setState({activeHighlight: null})}
    else {this.setState({activeHighlight: "conclusion"})};
    console.log(this.state.activeHighlight);

  }


  onNext(){
    let current = this.state;
    current = this.state.ix + 1;
    this.setState({ix:current});
    getArgData(this.user_id, this.argList[current]).then(data => this.setState({argData:data}));
    this.getAnnotations(this.argList[current])
      .then(annotation => {this.setState({annotation: annotation});
       this.validateForm();})
  }

  onPrev(){
    let current = this.state;
    current = this.state.ix - 1;
    this.setState({ix:current});
    getArgData(this.user_id, this.argList[current]).then(data => this.setState({argData:data}));
    this.getAnnotations(this.argList[current])
      .then(annotation => {this.setState({annotation: annotation});
       this.validateForm()})
  }

  onListClick(i){
    this.setState({ix: i});
    getArgData(this.user_id, this.argList[i]).then(data => this.setState({argData:data}));
    this.getAnnotations(this.argList[i])
      .then(annotation => {this.setState({annotation: annotation});
       this.validateForm();})
  };

  onSubmit(e) {
    if (e){
    e.preventDefault();
  }

    let wasSkipped =  this.state.skipped.includes(this.argList[this.state.ix])

    fetch('/api/submit-annotation', {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({arg_id: this.argList[this.state.ix], user: this.user, annotation: this.state.annotation, skipped: wasSkipped}),
        method: 'POST'
    });

    let completed = this.state.completed;
    completed.push(this.argList[this.state.ix]);
    this.setState({completed:completed});

    if (wasSkipped) {
      let skipped = this.state.skipped;
      skipped = skipped.filter(i => i != this.argList[this.state.ix]);
      this.setState({skipped:skipped});
    }

    if (this.state.ix < this.argList.length){
      this.onNext();
    };
}

onSelectType(event){
  let annotation = this.state.annotation;
  annotation['type'] = event.target.value;
  this.setState({annotation: annotation});
  this.validateForm()
}

onValid(event){
  let annotation = this.state.annotation;
  annotation['is_valid'] = event.target.value;
  this.setState({annotation: annotation});
  this.validateForm();
}


  canonicalForm(){
    let a, b, X, Y, R, annotation;
    switch(this.state.annotation.form) {
      case("alpha") :
        a = document.getElementById("a_form").value;
        Y = document.getElementById("Y_form").value;
        X = document.getElementById("X_form").value;

        annotation = this.state.annotation;
        annotation['conclusion'] =  a + " " + X;
        annotation['premise'] = a + " " + Y;
        annotation['canonical_form'] = {a:(a ? a : ""), y:(Y ? Y : ""), x:(X ? X : "")}
        this.setState({annotation:annotation});

        break;

      case("beta"):
        a = document.getElementById("a_form").value;
        b = document.getElementById("b_form").value;
        X = document.getElementById("X_form").value;

        annotation = this.state.annotation;
        annotation['conclusion'] =  a + " " + X;
        annotation['premise'] = b + " " + X;
        annotation['canonical_form'] = {a:(a ? a : ""), b:(b ? b : ""), x:(X ? X : "")}
        this.setState({annotation:annotation});

        break;

      case("gamma"):
        a = document.getElementById("a_form").value;
        b = document.getElementById("b_form").value;
        X = document.getElementById("X_form").value;
        Y = document.getElementById("Y_form").value;


        annotation = this.state.annotation;
        annotation['conclusion'] =  a + " " + X;
        annotation['premise'] = b + " " + Y;
        annotation['canonical_form'] = {a:(a ? a : ""), b:(b ? b : ""), y:(Y ? Y : ""), x:(X ? X : "")}
        this.setState({annotation:annotation});
        break;

      case("delta"):
        a = document.getElementById("a_form").value;
        R = document.getElementById("R_form").value;

        annotation = this.state.annotation;
        annotation['conclusion'] =  a;
        annotation['premise'] = a + " " + R;
        annotation['canonical_form'] = {a:(a ? a : ""), r:(R ? R : "")}
        this.setState({annotation:annotation});
        break;


    }
    this.validateForm()
  }

  onSkip(){
    let skipped = this.state.skipped;
    skipped.push(this.argList[this.state.ix]);
    this.setState({skipped:skipped});
    this.setSkipped(this.argList[this.state.ix]);

    if (this.state.completed.includes(this.argList[this.state.ix])){
      let completed = this.state.completed;
      completed = completed.filter(i => i != this.argList[this.state.ix]);
      this.setState({completed:completed});

      }

    if (this.state.ix <= this.argList.length - 1) {
      this.onNext();
    }
    else {
      this.onPrev();
    }
  }

  validateForm(){
    let canonical_form_completed = false;
    if (this.state.annotation.canonical_form){
      switch(this.state.annotation.form){
        case("alpha"):
          if (this.state.annotation.canonical_form.a &&
            this.state.annotation.canonical_form.x &&
            this.state.annotation.canonical_form.y) {
            canonical_form_completed = (this.state.annotation.canonical_form.a.trim().length > 1) &&
            (this.state.annotation.canonical_form.x.trim().length > 1) &&
            (this.state.annotation.canonical_form.y.trim().length > 1);
          }
          break;

        case("beta"):
          if (this.state.annotation.canonical_form.a &&
            this.state.annotation.canonical_form.x &&
            this.state.annotation.canonical_form.b){
          canonical_form_completed = (this.state.annotation.canonical_form.a.trim().length > 1) &&
          (this.state.annotation.canonical_form.x.trim().length > 1) &&
          (this.state.annotation.canonical_form.b.trim().length > 1);
        }
          break;

        case("gamma"):
          if (this.state.annotation.canonical_form.a &&
            this.state.annotation.canonical_form.x &&
            this.state.annotation.canonical_form.b &&
            this.state.annotation.canonical_form.y){
          canonical_form_completed = (this.state.annotation.canonical_form.a.trim().length > 1) &&
          (this.state.annotation.canonical_form.x.trim().length > 1) &&
          (this.state.annotation.canonical_form.b.trim().length > 1) &&
          (this.state.annotation.canonical_form.y.trim().length > 1);
        }
          break;

        case("delta"):
        if (this.state.annotation.canonical_form.a &&
          this.state.annotation.canonical_form.r){
          canonical_form_completed = (this.state.annotation.canonical_form.a.trim().length > 1) &&
         (this.state.annotation.canonical_form.r.trim().length > 1);
       }
          break;

        }
    }

    let validity_completed = (this.state.annotation.is_valid != null);

    if (this.state.annotation.type == "n/a"){
      if (validity_completed) {
        let annotation = this.state.annotation;
        annotation.is_valid = null;
        this.setState(annotation);
      }

      validity_completed = true;

    }

    this.setState({enableSubmit: this.state.annotation.prem_substance && this.state.annotation.conc_substance && canonical_form_completed && validity_completed});
  }



  render() {

    return (
        <div className="container-fluid">
        <div className="row">

            <div className="col-2 w-100">
                 <div className="btn-group w-100">
                 <button type="button" className="btn btn-primary" onClick={this.onPrev} disabled={(this.state.ix === 0) ? true : null}> Prev </button>
                 <button type="button" className="btn btn-secondary" onClick={this.onNext}  disabled={(this.state.ix === this.argList.length - 1) ? true : null}> Next </button>

                 </div>
                 <div className="progress border" style={{height: "50pt"}}>
                    <div className="progress-bar" role="progressbar" aria-valuenow={Math.round((this.state.completed.length + this.state.skipped.length - intersection(this.state.skipped, this.state.completed).length) / this.argList.length * 100)} aria-valuemin="0" aria-valuemax="100" style={{width:((this.state.completed.length + this.state.skipped.length - intersection(this.state.skipped, this.state.completed).length) / this.argList.length * 100).toFixed(2) + "%"}} >
	    {this.state.completed.length + this.state.skipped.length - intersection(this.state.skipped, this.state.completed).length} / {this.argList.length}
	    </div>
	    </div>


                 <div>
                <ScrollingList active={this.state.ix} rows={this.argList} handler={this.onListClick} completed={this.state.completed} skipped={this.state.skipped}/>
                </div>
               </div>


        <div className= "col-10">
        <div>
            <h1>ArgNotator</h1>

            <TextBox arg1={this.state.argData.text_to} arg2={this.state.argData.text_from} premise={this.state.annotation.highlightedPremise} conclusion={this.state.annotation.highlightedConclusion} topic={this.state.argData.topic} />

            </div>

            <HighlightButtons premiseHandler={this.onClickHighlightPremise} conclusionHandler={this.onClickHighlightConclusion} active={this.state.activeHighlight}/>

            <form method='POST' onSubmit={this.onSubmit}>

            <div className="form-view">

            <ArgumentForm handler={this.onFormChange} form={this.state.annotation.form}/>

            <div className="form-group">
            {(() => {
              switch (this.state.annotation.form) {
                case "alpha":   return <AlphaRewrite handler={this.canonicalForm} value={this.state.annotation.canonical_form}/>;
                case "beta": return <BetaRewrite handler={this.canonicalForm} value={this.state.annotation.canonical_form}/>;
                case "gamma":  return <GammaRewrite handler={this.canonicalForm} value={this.state.annotation.canonical_form}/>;
                case "delta":      return <DeltaRewrite handler={this.canonicalForm} value={this.state.annotation.canonical_form}/>;
              }
             })()}
             </div>


             <div className="container-fluid rounded bg-light">
             <span style={{color:"indigo"}}> {(this.state.annotation.conclusion == "") ? "Conclusion " : this.state.annotation.conclusion} </span>
             <span style={{color:"gray-100"}}> because, </span>
             <span style={{color:"purple"}}> {(this.state.annotation.premise == "") ? "premise." : this.state.annotation.premise} </span>
             </div>

             <div>
             <Substance title="Conclusion" name="conclusion_substance" handler={this.onSubstanceChange} substance={this.state.annotation.conc_substance} />
             </div>

          <div>
            <Substance title="Premise" name="premise_substance" handler={this.onSubstanceChange} substance={this.state.annotation.prem_substance} />

                </div>

          <div>
          <TypeDropdown types={argTypes(this.state.annotation.form, this.state.annotation.conc_substance, this.state.annotation.prem_substance)} handler={this.onSelectType} value={this.state.annotation.type} />
          </div>

          <Lever type={this.state.annotation.type} form={this.state.annotation.form} canonical_form={this.state.annotation.canonical_form} is_valid={this.state.annotation.is_valid} handler={this.onValid}/>
                <input className="form-control" disabled={!this.state.enableSubmit} type="submit"/>
                </div>
                </form>

                 <div className="position-absolute bottom-0 end-0"><button type="button" className="btn btn-danger" onClick={this.onSkip}>Skip</button></div>



                </div>
            </div>
            </div>


      )
    }
}

export default Argnotator;
