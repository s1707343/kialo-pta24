import React, { Component} from 'react';

import Argnotator from './Argnotator/Argnotator';
import Login from './Login/Login';



class App extends Component {
  constructor(props){
    super(props);
    this.state = {username: null, id: null}
    this.submitUsername = this.submitUsername.bind(this);
  }

  componentDidMount() {
    this.setState({username: sessionStorage.getItem('username')})
    this.setState({id: sessionStorage.getItem('id')})

  }

  submitUsername(name, id) {
    sessionStorage.setItem('username', name)
    sessionStorage.setItem('id', id)

    this.setState({username: name, id:id})
  }

  render() {
    let view = !this.state.username ? <Login handler={this.submitUsername}/> : <Argnotator user={this.state.username} user_id={this.state.id}/>
    return (
      view
  )
  }
}

export default App;
