import React from 'react';


async function checkUserExists(username) {
  return ( await fetch(`api/exists-user?name=${username}`)
        .then(response => response.json())
      )
}




function Form(props){

  function submitForm(event) {
    event.preventDefault();
    const username = document.getElementById("username").value;
    checkUserExists(username).then(response => {
      if (response['exists']) {
        props.handler(username, response['id'])
      }
      else (document.getElementById("username").value = '')
    })
  }

  return (
  <div className="container mh-100 ">
  <div className="row h-100 justify-content-center align-items-center ">
  <h2> ArgNotator </h2>
  </div>
  <div className="row justify-content-center align-items-center ">
    <form className="col-12 my-auto">
      <div className="form-group">
        <input type="text" className="form-control" id="username" placeholder="Username" />
        <button type="submit" className="form-control" id="submit-button" onClick={submitForm}> Log in </button>
      </div>
    </form>
  </div>
</div>
)

}

export default function Login(props) {
  return(
    <Form handler={props.handler}/>
  );
}
