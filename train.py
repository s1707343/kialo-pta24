import os
from argparse import ArgumentParser

import pandas as pd
import numpy as np
import evaluate
import nltk

from transformers import LlamaForCausalLM, LlamaTokenizer
from transformers import AutoModelForSeq2SeqLM, AutoTokenizer, TrainingArguments, DataCollatorWithPadding, Seq2SeqTrainer, Seq2SeqTrainingArguments
from trl import DataCollatorForCompletionOnlyLM, SFTTrainer
from transformers.generation.logits_process import LogitsProcessor
from datasets import Dataset, DatasetDict
from transformers_cfg.grammar_utils import IncrementalGrammarConstraint
from transformers_cfg.generation.logits_process import GrammarConstrainedLogitsProcessor
import json

def preprocess_function(examples, tokenizer):    
    model_inputs = tokenizer(examples["text_from"], examples["text_to"], truncation=True, padding=True)
    labels = []
    for form in  examples['canonical_form']:
        form = {key: form[key].replace("\"", "\'") for key in ('a', 'x', 'y')}
        form = json.dumps(form).replace('\n', '')
        labels.append(form)
    labels = tokenizer(labels, padding=True, truncation=True)
    model_inputs['labels'] = labels['input_ids']
    return model_inputs

def llama_preprocessor(examples):
    outputs = []
    for i in range(len(examples['text_to'])):
        text = f"### Rewrite the following argument in canonical form a is X because a is Y, give your answer as a dictionary {{\"a\": a, \"x\": x, \"y\": y, }} :\n Premise: {examples['text_to'][i]}\nConclusion: {examples['text_from'][i]}\n ### Answer: {json.dumps(examples['canonical_form'][i]).replace('\n', ' ')}"
        outputs.append(text)
    return outputs
    



metric = evaluate.load("rouge")


def compute_metrics(eval_pred):
    predictions, labels = eval_pred
    decoded_preds = tokenizer.batch_decode(predictions, skip_special_tokens=True)
    # Replace -100 in the labels as we can't decode them.
    labels = np.where(labels != -100, labels, tokenizer.pad_token_id)
    decoded_labels = tokenizer.batch_decode(labels, skip_special_tokens=True)
    
    # Rouge expects a newline after each sentence
    decoded_preds = ["\n".join(nltk.sent_tokenize(pred.strip())) for pred in decoded_preds]
    decoded_labels = ["\n".join(nltk.sent_tokenize(label.strip())) for label in decoded_labels]
    
    # Note that other metrics may not have a `use_aggregator` parameter
    # and thus will return a list, computing a metric for each sentence.
    result = metric.compute(predictions=decoded_preds, references=decoded_labels, use_stemmer=True, use_aggregator=True)
    # Extract a few results
    result = {key: value * 100 for key, value in result.items()}
    # Add mean generated length
    prediction_lens = [np.count_nonzero(pred != tokenizer.pad_token_id) for pred in predictions]
    result["gen_len"] = np.mean(prediction_lens)
    
    return result



if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("--model", default="google/flan-t5-large")
    parser.add_argument("--form", type=str, default='alpha')

    args = parser.parse_args()

    df = pd.read_pickle('data/kialo-pta24.pkl')
    df = df[df['form']==args.form]
    dataset = {}
    dataset['train'] = Dataset.from_pandas(df[df['split'] == 'train'])
    dataset['val'] = Dataset.from_pandas(df[df['split'] == 'val'])
    dataset = DatasetDict(dataset)

    model_name = args.model.split('/')[-1]


    if "llama" in model_name.lower():
        model = LlamaForCausalLM.from_pretrained(args.model, device_map='auto')
        tokenizer = LlamaTokenizer.from_pretrained(args.model, add_eos_token=True)
        tokenizer.add_special_tokens({'pad_token': '[PAD]'})

        model.resize_token_embeddings(len(tokenizer))
        response_template_ids = tokenizer.encode("\n ### Answer: ", add_special_tokens=False)[2:-1]  # Now we have it like in the dataset texts: `[2277, 29937, 4007, 22137, 29901]`

        data_collator = DataCollatorForCompletionOnlyLM(response_template_ids , tokenizer=tokenizer)
        MAX_LEN = 300
        BATCH_SIZE=1

        training_args = TrainingArguments(
            output_dir=f"forms+{model_name}",
            learning_rate=1e-4,
            per_device_train_batch_size=BATCH_SIZE,
            per_device_eval_batch_size=BATCH_SIZE,
            num_train_epochs=10,
            weight_decay=0.01,
            evaluation_strategy = 'epoch',
            logging_steps = 10,
            logging_dir = f"forms+{model_name}/logs",
            save_strategy='epoch',
            )

    
        trainer = SFTTrainer(
            model=model,
            args=training_args,
            train_dataset=dataset['train'],
            eval_dataset=dataset['val'],
            data_collator=data_collator,
            tokenizer=tokenizer,
            formatting_func=llama_preprocessor
            ) 

    else:
        model = AutoModelForSeq2SeqLM.from_pretrained(args.model)
        tokenizer = AutoTokenizer.from_pretrained(args.model)
        tokenizer.add_tokens(['{', '}'])

        data_collator = DataCollatorWithPadding(tokenizer=tokenizer)
        dataset = dataset.map(lambda x: preprocess_function(x, tokenizer), batched=True)

        MAX_LEN = 100
        BATCH_SIZE=8


        training_args = Seq2SeqTrainingArguments(
                output_dir=f"forms+{model_name}",
                learning_rate=1e-4,
                per_device_train_batch_size=BATCH_SIZE,
                per_device_eval_batch_size=BATCH_SIZE,
                num_train_epochs=20,
                weight_decay=0.01,
                evaluation_strategy = 'epoch',
                logging_steps = 10,
                logging_dir = f"forms+{model_name}/logs",
                save_strategy='epoch',
                predict_with_generate=True,
                generation_max_length=MAX_LEN,
                generation_num_beams=2
                )

    
        trainer = Seq2SeqTrainer(
            model=model,
            args=training_args,
            train_dataset=dataset['train'],
            eval_dataset=dataset['val'],
            compute_metrics=compute_metrics,
            data_collator=data_collator,
            tokenizer=tokenizer,
            ) 


    trainer.train()

