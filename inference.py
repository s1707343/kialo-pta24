import os
from argparse import ArgumentParser

import pandas as pd
import numpy as np
import evaluate
import nltk
import torch
import transformers
from tqdm import tqdm
import json

from transformers import LlamaForCausalLM, LlamaTokenizer
from transformers import AutoModelForSeq2SeqLM, AutoTokenizer, DataCollatorWithPadding, Seq2SeqTrainer, Seq2SeqTrainingArguments
from transformers.generation.logits_process import LogitsProcessor
from datasets import Dataset, DatasetDict
from transformers_cfg.grammar_utils import IncrementalGrammarConstraint
from transformers_cfg.generation.logits_process import GrammarConstrainedLogitsProcessor


def preprocess_function(examples, tokenizer):    
    model_inputs = tokenizer(examples["text_from"], examples["text_to"], truncation=True, padding=True, return_tensors='pt')
    return model_inputs

def preprocess_few_shot(examples, tokenizer):
    model_inputs = tokenizer(examples["text"], truncation=True, padding=True, return_tensors="pt")
    return model_inputs

def llama_preprocessor(examples, train_examples):
    train_text = ""
    if train_examples:
        train_text = "Here are a few examples:\n"
        for i in range(len(train_examples['text_from'])):
            text = f"Premise: {train_examples['text_to'][i]}\nConclusion: {train_examples['text_from'][i]}\nAnswer: {train_examples['canonical_form'][i]}\n"

    outputs = []
    if type(examples) != list:
        text = f"### Rewrite the following argument in the form a is X because a is Y, give your answer in JSON format {{\'a\': a, \'y\': y, \'x\': x }} :\n {train_text} Premise: {examples['text_to']}\nConclusion: {examples['text_from']}\n ### Answer: "
        outputs.append(text)
    examples['text'] = outputs
    return examples


metric = evaluate.load("rouge")

def compute_metrics(eval_pred):
    predictions, labels = eval_pred
    decoded_preds = tokenizer.batch_decode(predictions, skip_special_tokens=True)
    # Replace -100 in the labels as we can't decode them.
    labels = np.where(labels != -100, labels, tokenizer.pad_token_id)
    decoded_labels = tokenizer.batch_decode(labels, skip_special_tokens=True)
    
    # Rouge expects a newline after each sentence
    decoded_preds = ["\n".join(nltk.sent_tokenize(pred.strip())) for pred in decoded_preds]
    decoded_labels = ["\n".join(nltk.sent_tokenize(label.strip())) for label in decoded_labels]
    
    # Note that other metrics may not have a `use_aggregator` parameter
    # and thus will return a list, computing a metric for each sentence.
    result = metric.compute(predictions=decoded_preds, references=decoded_labels, use_stemmer=True, use_aggregator=True)
    # Extract a few results
    result = {key: value * 100 for key, value in result.items()}
    # Add mean generated length
    prediction_lens = [np.count_nonzero(pred != tokenizer.pad_token_id) for pred in predictions]
    result["gen_len"] = np.mean(prediction_lens)
    
    return result

if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("--dataset", default="dataset.hf")
    parser.add_argument("--model", default="google/flan-t5-large")
    parser.add_argument("--checkpoint", default="0", type=int)
    parser.add_argument("--n_shots", default=0, type=int)

    args = parser.parse_args()



    df = pd.read_pickle('data/kialo-pta24.pkl')
    test_ds = Dataset.from_pandas(df[df['split']=='train'])

    train_ds = Dataset.from_pandas(df[df['split']=='test'])
    dataset = DatasetDict({'train':train_ds, 'test':test_ds})
    model_name = args.model.split('/')[-1]

    if args.n_shots == 0:
        checkpoint = f"forms+{model_name}/checkpoint-{args.checkpoint}"
        train_examples = None

    else:
        checkpoint = args.model
        total = dataset['train'].num_rows
        indices = rng.choice(np.arange(0, total), size=args.n_shots)
        train_examples = dataset['train'].select(indices.tolist())
 
    if "llama" in model_name.lower():
        pipeline = transformers.pipeline(
            "text-generation",
            model=checkpoint,
            torch_dtype=torch.float16,
            device_map="auto",
        )
        tokenizer = LlamaTokenizer.from_pretrained(checkpoint, add_eos_token=False)

        dataset = dataset.map(lambda x: llama_preprocessor(x, train_examples), batched=False)
        eval_preds = []
        for example in tqdm(dataset['test']['text']):
            prediction = pipeline(example,
                do_sample=True,
                num_beams=2, 
                num_return_sequences=1,
                eos_token_id=tokenizer.eos_token_id,
                return_full_text=False,
                max_new_tokens=100
            )
            prediction = prediction[0][0]['generated_text']
            eval_preds.append(prediction)
    else:
        model = AutoModelForSeq2SeqLM.from_pretrained(checkpoint)
        tokenizer = AutoTokenizer.from_pretrained(checkpoint)

        data_collator = DataCollatorWithPadding(tokenizer=tokenizer)
        if args.n_shots > 0:
            dataset = dataset.map(lambda x: llama_preprocessor(x, train_examples), batched=False)
            dataset = dataset.map(lambda x: preprocess_few_shot(x, tokenizer), batched=True)
        else:
            dataset = dataset.map(lambda x: preprocess_function(x, tokenizer), batched=True)

        MAX_LEN = 100
        BATCH_SIZE=8

        eval_preds = []
        for example in tqdm(dataset):
            model_inputs = torch.tensor(example['input_ids'])
            if model_inputs.shape[0] != 1:
                model_inputs = model_inputs.unsqueeze(0)
            output = model.generate(model_inputs, max_new_tokens=100, num_beams=2, repetition_penalty=2.0, early_stopping=True)
            output = tokenizer.decode(output[0], skip_special_tokens=True)
            eval_preds.append(output)

    with open(f'{model_name}-{args.n_shots}_inference.txt', 'w') as f:
        for prediction in eval_preds:
                f.write(prediction.replace('\n', ' ') + '\n')

    labels = map(json.dumps, dataset['canonical_form'])
    eval_results = compute_metrics(zip(eval_preds, labels))
    print(eval_results)
