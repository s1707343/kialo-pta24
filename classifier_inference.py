from argparse import ArgumentParser

import pandas as pd
import numpy as np

from sklearn.metrics import precision_recall_fscore_support, accuracy_score

from transformers import AutoModelForSequenceClassification, AutoTokenizer, DataCollatorWithPadding, Trainer, TrainingArguments
from transformers.generation.logits_process import LogitsProcessor
from datasets import Dataset, DatasetDict
from tqdm import tqdm

def preprocess_function(examples):    
    model_inputs = tokenizer(examples["text"],truncation=True, padding=True)
    # labels = tokenizer(examples['labels'], padding=True, truncation=True)
    model_inputs['labels'] = examples['labels']
    return model_inputs





def compute_metrics(eval_pred):
    predictions, labels = eval_pred

    predictions = np.argmax(predictions, axis=-1)
    results = {}
    p, r, f1, _ = precision_recall_fscore_support(labels, predictions, average='macro')
    acc = accuracy_score(labels, predictions)
    results['precision'] = p
    results['recall'] = r
    results['f1'] = f1
    results['accuracy'] = acc
    # Extract a few results

    results = {key: value * 100 for key, value in results.items()}
    # Add mean generated length
    
    return results



if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--model", default="bert-large-uncased")
    parser.add_argument("--checkpoint", default="10770")
    
    args = parser.parse_args()

    df = pd.read_pickle('data/kial-pta24.pkl')
    df = df[['split']=='test']
    df1 = df.drop(['conc_substance', 'conclusion'], axis=1).rename({'prem_substance':'substance', 'premise':'rewritten_text'}, axis=1)
    df2 = df.drop(['prem_substance', 'premise'], axis=1).rename({'conc_substance':'substance', 'conclusion':'rewritten_text'}, axis=1)
    df = pd.concat([df1, df2], axis=0)

    dataset = DatasetDict.load_from_disk(args.dataset)

    model_name = args.model.split('/')[-1]
    checkpoint = f"classification+{model_name}/checkpoint-{args.checkpoint}"

    model = AutoModelForSequenceClassification.from_pretrained(checkpoint, num_labels=3)
    tokenizer = AutoTokenizer.from_pretrained(checkpoint)

    tokenized_dataset = dataset.map(preprocess_function, batched=True)

    data_collator = DataCollatorWithPadding(tokenizer=tokenizer)


    training_args = TrainingArguments(
            output_dir="classification+"+model_name,
            per_device_train_batch_size=16,
            per_device_eval_batch_size=32,
            logging_dir = f"classification+{model_name}/logs",
            )


    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=tokenized_dataset['train'],
        eval_dataset=tokenized_dataset['test'],
        compute_metrics=compute_metrics,
        data_collator=data_collator,
        tokenizer=tokenizer,
        )

    metrics = trainer.evaluate() 
    print(metrics)
    

