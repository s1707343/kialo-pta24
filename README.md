# Kialo-PTA24

 This repostory contains code, data and training materials from the paper Beyond Recognising Entailment: Formalising Natural Language Inference From an Argumentative Perspective [INSERT LINK]
## Data Description

The dataset can be found as a pickle dump in `data/kialo-pta24.pkl`. The original annotation data taken from [Jo et. al (2021)](https://aclanthology.org/2021.tacl-1.44/) can be found in `ArgNotator/data/annotation_data.csv`.


|Column|Description|
|------|-----------|
|pairid | unique identifiers of propositions|
|topic | title of kialo discussion|
|text_to|main claim|
|text_from|supporting claim|
|form |alpha/beta/gamma/delta|
|highlightedPremise | part of original text containing the premise| 
|highlightedConclusion | part of original text containing the conclusion| 
|premise | rewritten premise| 
|conclusion | rewritten conclusion| 
|canonical_form |dict of canonical form components e.g. {'a': "dogs", 'x': "should be treated with kindness", 'y': "are loyal creatures"}|
|prem_substance|substance of premise: policy/value/fact|
|conc_substance|substance of conclusion: policy/value/fact|
|type|PTA argument type|
|is_valid|validity of the argument lever: yes/no/N/A|
|split|train/val/test|



## Reproducing Paper Results
### Setup

1. Create and activate new conda environment with python 3.12
2. Run `pip install torch transformers[torch] datasets evaluate`
3. Install additional dependencies `pip install -r requirements.txt`

### Substance Classification

1. Run `python train_classifier.py --model_name MODEL_NAME`, where MODEL_NAME is any valid huggingface model with a classification head
2. View validation curve using `tensorboard --logdir=classification+model_name` to determine the best checkpoint
3. Run `python classifier_inference.py --model_name MODEL_NAME --checkpoint CHECKPOINT`, where CHECKPOINT is simply the number corresponding to the best checkpoint

### Alpha Argument Canonicalisation

1. Run `python train.py --model_name MODEL_NAME`, where MODEL_NAME is either any huggingface Seq2Seq model or a Llama model, you may also experiment with other argument forms using the `--forms` argument.

2. View validation curve using `tensorboard --logdir=forms+model_name` to determine the best checkpoint. In our experiments we choose the model with the best validation Rouge-L.

3. Run `python inference.py --model_name MODEL_NAME --checkpoint CHECKPOINT`, where CHECKPOINT is simply the number corresponding to the best checkpoint. 

4. For few-shot inference, simply add the flag `--n-shots=N` for some integer `N` to the previous command.


## ArgNotator
Details on running the annotation tool can be found in ArgNotator/README.md
